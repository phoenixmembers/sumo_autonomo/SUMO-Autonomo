#import "TimerOne.h"
//Defines copiados do codigo do lego

// Valor referente a cada sensor
#define US_SENSOR_LEFT 2
#define US_SENSOR_RIGHT 1

// Alcance máximo considerado pelo programa ***********
#define RANGE 53 //80 max power 20 kp

// Máxima porcentagem de energia utilizado pelo motor em linha reta
#define MAX_POWER 17 //****************

#define FRENTE 1
#define TRAS -1

// Valores lógicos de direita e esquerda
#define RIGHT 1
#define LEFT -1

//Constantes PID
#define Kp 3
#define Ki 0
#define Kd 0

// Distancia entre centro dos sensores em cm
#define WIDTH 9
//Fim dos defines copiados


//Pin 2 recebe saida do 7408(AND) dos sensores da frente
//Pin 3 recebe saida do 7408(AND) dos sensores de tras

//Pinos de echo e trigger dos ultrasonicos direito e esquerdo
#define ULTRASOM_ESQ_ECHO 7
#define ULTRASOM_ESQ_TRIG 6
#define ULTRASOM_DIR_ECHO 5
#define ULTRASOM_DIR_TRIG 4

//Macros que permitem o uso das funcoes LEGO sem alteracao
#define SensorUS(SENSOR) ((SENSOR == US_SENSOR_RIGHT)?SensorUS_Ard(ULTRASOM_DIR_TRIG, ULTRASOM_DIR_ECHO):SensorUS_Ard(ULTRASOM_ESQ_TRIG, ULTRASOM_ESQ_ECHO))
#define OnRev(MOTOR,POW) ((MOTOR == MOTOR_RIGHT)?OnRev_Barras(MOTOR, POW):OnRev_SemBarras(MOTOR, POW))
#define OnFwd(MOTOR,POW) ((MOTOR == MOTOR_RIGHT)?OnFwd_Barras(MOTOR, POW):OnFwd_SemBarras(MOTOR, POW)) //IMPORTANTE: Motor direito eh agora o com codigo de barras
#define Off(MOTOR) ((MOTOR == MOTOR_RIGHT)?Off_Barras(MOTOR):Off_SemBarras(MOTOR))

//Pinos dos motores
#define VICTOR_D 10
#define VICTOR_E 9
#define MOTOR_RIGHT VICTOR_D
#define MOTOR_LEFT VICTOR_E

//Pino de direcao inicial de rotação
#define ROTATION_DIRECTION 11

#define ON_OFF 12

//mutex stopMutex;  // Mutex de proteção da variável stopRobot
boolean stopRobot = false; // Indica a parada do robô por detecção de sensores
//mutex resetMutex; // Mutex de proteção da variável resetState
boolean resetState = true; // Indica se o robô encontra-se em estado de reset
int lastRotationDirection = RIGHT; // Último lado de rotação do robô

boolean shutDown = false;

float P=0, I=0, D=0, lastP = 0;
boolean onAction = false;
boolean shouldStop = false;
int sLeft = 255, sRight = 255;

int rotating = 0;

void setup() //Configuracao de pinos
{
  Serial.begin(9600);
  //Linha
  attachInterrupt(0, frente,FALLING);//Seta interrupt de linha da frente
  attachInterrupt(1, tras,FALLING);//Seta interrupt de linha de tras
  pinMode(2,INPUT);
  pinMode(3,INPUT);
  //Fim linha
  //Victor
  Timer1.initialize(10);
  configMotor(VICTOR_D);
  configMotor(VICTOR_E);
  //Fim Victor
  //Ultrasom
  SetSensorLowspeed(ULTRASOM_DIR_TRIG, ULTRASOM_DIR_ECHO);
  SetSensorLowspeed(ULTRASOM_ESQ_TRIG, ULTRASOM_ESQ_ECHO);
  
    
  //Fim ultrasom
}

int bateria = 0;


void readInitialDirection(){
   if(digitalRead(ROTATION_DIRECTION)){
      lastRotationDirection = RIGHT; 
   }else{
     lastRotationDirection = LEFT;
   }
}

void loop()
{
  /* Faz o controle do motor  em 3 partes:

   1ª: Se estamos em um momento de reset (início do round ou quando nos aproximamos da linha).
       O robô gira até que um de seus sensores encontra o oponente
   2ª: Quando o robô está em modo ativo de perseguição.
       Através de controle PID (ler para melhor se informar http://www.inpharmix.com/jps/PID_Controller_For_Lego_Mindstorms_Robots.html)
       é feito para melhor perseguir o oponente sem precisar parar.
       No caso de anulações de leituras (ou muito próximas ou muito longe) o robô repetirá a leitura anterior.
   3ª: Foi acionado stopRobot logo reset o ciclo do controle de motores.
   
   O controle do motor está somente P por preguiça de calcular as outras constantes.

*/
  /*Off(MOTOR_RIGHT);
  Off(MOTOR_LEFT);
  delay(5000);
  //rotate(lastRotationDirection);
  OnFwd(MOTOR_LEFT,50);
  OnFwd(MOTOR_RIGHT,50);
  delay(5000);
 if(!rotating)
   rotate(lastRotationDirection);
    
   rotating = 1;
   
   OnFwd(MOTOR_LEFT,100);
   OnFwd(MOTOR_RIGHT,100);*/
   
   
  while(digitalRead(ON_OFF));
   
  if(!bateria)
    delay(10000);
    
   bateria = 1;
   
  
  
    readInitialDirection();

 while(!onAction&&!shouldStop){

      rotate(lastRotationDirection);
      
      if(!digitalRead(ON_OFF))
               shouldStop = true;
      
      while (!isSensorReady());
      sLeft = SensorUS(US_SENSOR_LEFT);
      sRight = SensorUS(US_SENSOR_RIGHT);
      Serial.print(sLeft);
      Serial.print("\t");
      Serial.println(sRight);

      if(sLeft <= RANGE || sRight <= RANGE){
             onAction = true;
             resetState = false;
             //PlaySound(SOUND_CLICK);
      }

  }


  P = 0;
  I = 0;
  D = 0;
  lastP = 0;
  
  while(digitalRead(ON_OFF));

  shouldStop = false;
  stopRobot = false;

  while (!shouldStop){

       float width = WIDTH;

       if(sRight > sLeft + width){
           sRight = RANGE;
       }else if(sLeft > sRight + width){
           sLeft = RANGE;
       }

       if(sRight > RANGE){
           sRight = RANGE;
       }

       if(sLeft > RANGE){
           sLeft = RANGE;
       }


       if(sLeft != RANGE || sRight != RANGE){
                // Distancia relativa ao centro dos sensores em que o centro da leitura dos sensores aproximadamente se encontra;

                float z = (-1*(RANGE - sLeft) +(RANGE - sRight))/(2*RANGE - sLeft - sRight);

                lastP = P;
                P = z;

                //NumOut(0, LCD_LINE1, z);

                I = I + P;

                D = P - lastP;

       }else{
             lastP = P;
             //P = P;
       }

       float correctionValue = Kp*P + Ki*I + Kd*D;


       if (correctionValue > 0){
            lastRotationDirection = RIGHT;
       }else{
             lastRotationDirection = LEFT;
       }

        int powerRight = MAX_POWER - correctionValue;
        int powerLeft = MAX_POWER + correctionValue;

        OnFwd(MOTOR_RIGHT,powerRight);
        OnFwd(MOTOR_LEFT,powerLeft);

        shouldStop = stopRobot;

      if(!digitalRead(ON_OFF))
         shouldStop = true; 

     if(!shouldStop){
           while (!isSensorReady());
           sLeft = SensorUS(US_SENSOR_LEFT);
           sRight = SensorUS(US_SENSOR_RIGHT);
     }

  }

  if(linha() == FRENTE){
    OnRev(MOTOR_RIGHT,100);
    OnRev(MOTOR_LEFT,100);
    Serial.print("Frente\n");
    
  }else if(linha() == TRAS){
    OnFwd(MOTOR_RIGHT,100);
    OnFwd(MOTOR_LEFT,100);
    Serial.print("Tras\n");
  }
  zeraLinha();
  delay(100);
  Off(MOTOR_RIGHT);  
  Off(MOTOR_LEFT);
}
// Rotaciona o robô em determinada direção
void rotate(int direction){

        if (direction == RIGHT){
           /*OnRev(MOTOR_RIGHT,100);
           OnFwd(MOTOR_LEFT,100);*/
           OnRev(MOTOR_RIGHT,17);
           OnFwd(MOTOR_LEFT,17);
        }else{
           /*OnFwd(MOTOR_RIGHT,100);
           OnRev(MOTOR_LEFT,100);*/
           OnFwd(MOTOR_RIGHT,17);
           OnRev(MOTOR_LEFT,17);
        }

}
