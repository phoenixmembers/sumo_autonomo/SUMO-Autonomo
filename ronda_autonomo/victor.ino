void configMotor(uint8_t motorPin)
{
  pinMode(motorPin, OUTPUT);
  Timer1.pwm(motorPin, 92, 17000);
}
void OnRev_SemBarras(uint8_t motorPin, int8_t pwm)
{
  OnFwd_SemBarras(motorPin, -pwm);
}
void OnRev_Barras(uint8_t motorPin, int8_t pwm)
{
  OnFwd_Barras(motorPin, -pwm);
}
//Barras e sem barras eh o que diferencia os victors. Como a calibragem eh diferente
//as funcoes sao difetentes
void OnFwd_SemBarras(uint8_t motorPin, int8_t pwm)//Zero em 91
{
   
  int dutyCycle = (60*(pwm+100))/200.0 + 61; //Duty Cycle 
  Timer1.setPwmDuty(motorPin, dutyCycle);
}
void OnFwd_Barras(uint8_t motorPin, int8_t pwm)//Zero em 94
{
   
  int dutyCycle = (60*(pwm+100))/200.0 + 64; //Duty Cycle 
  Timer1.setPwmDuty(motorPin, dutyCycle);
}
void Off_SemBarras(uint8_t motorPin)
{
  Timer1.setPwmDuty(motorPin, 91);
}
void Off_Barras(uint8_t motorPin)
{
  Timer1.setPwmDuty(motorPin, 94);
}

