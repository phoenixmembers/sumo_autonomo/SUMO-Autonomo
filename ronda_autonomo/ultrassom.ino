/*
  Funcoes para o funcionamento dos sensores.
  Supoe-se que os sensores sejam independentes, ou seja,
  as entradas dos triggers sao diferentes. Assim deve-se mandar os pinos onde se encontram
  a entrada so trigger e a saida do echo.
  A funcao waitSensorUS existe e deve ser usada depois da leitura dos dois
  sensores. Talvez com a velocidade de execucao do programa ela nao seja necessaria, mas
  eh importante pois o sensor de ultrassom necessita de ao menos um delay de
  60 milisegundos
  para o envio do pulso do trigger.
*/
#define TEMPO_ENTRE_LEITURAS 60 //tempo em millis entre uma leitura e outra
unsigned long timeStampUS; //Hora de chamada da leitura do US

//Funcao para enviar o pulso do Ultrassom
long SensorUS_Ard(int trigPin, int echoPin){//A para diferencias da macro criada em ronda_autonomo. Macro criada para  nao ter que alterar codigo do lego
  long duration, distance;
  
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);//Envia um pulso de 10 us no trigger input
  digitalWrite(trigPin, LOW);

  duration = pulseIn(echoPin, HIGH);
  //distance = (duration / 2 ) / 29.1;
  distance = (duration / 58 );//Datasheet
  
  timeStampUS = millis();
  
  return distance;  
}

//Funcao que verifica se sensor ja pode ser lido novamente
boolean isSensorReady()
{
  if ((timeStampUS - millis()) > TEMPO_ENTRE_LEITURAS) return true;
  else return false;
}
//Funcao para esperar ateh que se possa enviar um novo  
void waitSensorUS(){
   delayMicroseconds(60000); 
}

//Inicializa o sensor Ultrasom.
void SetSensorLowspeed(int trigPin, int echoPin){
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
}

